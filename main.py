from environment import Environment

SENSORS = 50
ACTUATORS = 100

def main():
	env = Environment(SENSORS, ACTUATORS, (10000, 10000))
	env.step()

if __name__ == '__main__':
	main()