from mesa import Agent
from mesa import space
from sensor import Sensor
import utils
import torch
import numpy as np

K = 20
MAP_SHAPE = (9,9)

class Actuator(Agent):
	def __init__(self, unique_id, model, pos):
		super().__init__(unique_id, model)
		self.pos = pos
		self.prev_map = None
		self.prev_actuation = None

	def step(self):
		neig = self.model.environment.get_neighbors(self.pos, K, include_center=False)

		sensors = list(filter(lambda x: type(x) is Sensor, neig))
		ids = list(map(lambda x: x.id, sensors))
		values = list(map(lambda x: x.value, sensors))
		positions = list(map(lambda x: x.pos, sensors))

		heatmap, deltamap = build_maps(values, positions)

		power = actuate(heatmap, deltamap)
		learn(heatmap, deltamap)
		prev_actuation = power

	def actuate(self, heatmap, deltamap):
		power = self.model.forward(heatmap, deltamap)
		self.sim.actuate(power, self.pos)
		return power

	def learn(self, heatmap, deltamap):
		self.lodel.train(heatmap, deltamap, prev_actuation)

	def build_maps(self, values, positions):
		heatmap = np.zeros(MAP_SHAPE)
		deltamap = np.zeros(MAP_SHAPE)

		for point in np.nditer(heatmap, op_flags=['readwrite']):
			sumv = 0
			sumd = 0
			for (v, p) in zip(values, positions):
				d = utils.dist(self.pos, p)
				sumd += d
				sumv += v * d

			point = sumv / sumd

		if prev_map not None:
			deltamap = prev_map - heatmap

		prev_map = heatmap

		return (heatmap, deltamap)