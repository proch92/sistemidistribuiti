import numpy as np
import random

DENSITY = 1000 # 1000x1000 point matrix
REBALANCE_FACTOR = 0.15

class Sim(object):
	"""Environment simulation"""
	def __init__(self, shape):
		super(Sim, shape).__init__()
		self.shape = shape
		ether = np.zeros((DENSITY, DENSITY))

	def step(self):
		perturbate()
		ether_rebalance()