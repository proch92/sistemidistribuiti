from mesa import Agent
from mesa import space
import torch


class Sensor(Agent):
	def __init__(self, unique_id, model, pos):
		super().__init__(unique_id, model)
		self.pos = pos
		self.previous_read = None
		self.value = 0
		self.delta = 0

	def step(self):
		self.value = self.read_sensor()
		self.delta = calc_delta(value) # delta = value - prev

	def read_sensor(self):
		return 0