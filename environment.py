from mesa import Model, time, space
from sensor import Sensor
from actuator import Actuator
import random


class Environment(Model):
	def __init__(self, SENSORS, ACTUATORS, space_shape):
		self.space_shape = space_shape
		self.num_sensors = SENSORS
		self.num_actuators = ACTUATORS
		self.scheduler = time.RandomActivation(self)
		self.environment = space.ContinuousSpace(self.space_shape[0], self.space_shape[1], torus = False)

		for i in range(self.num_sensors):
			pos = self.random_location2D()
			sensor = Sensor(i, self, pos)
			self.scheduler.add(sensor)
			self.environment.place_agent(sensor, pos)

		for i in range(self.num_actuators):
			pos = self.random_location2D()
			actuator = Actuator(i, self, pos)
			self.scheduler.add(actuator)
			self.environment.place_agent(actuator, pos)

	def step(self):
		self.scheduler.step()

	def random_location2D(self):
		x = random.uniform(0, self.space_shape[0])
		y = random.uniform(0, self.space_shape[1])
		return (x,y)